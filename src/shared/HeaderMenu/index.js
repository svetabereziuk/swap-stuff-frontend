import React, { Component } from "react";
import HeaderMenu from "./HeaderMenu";

const menuItems = [
  {
    link: "/",
    title: "Home"
  },
  {
    link: "/products",
    title: "Products"
  },
  {
    link: "/signin",
    title: "Sign in"
  },
];

export default class HeaderMenuContainer extends Component {
  render() {
    return <HeaderMenu menu={menuItems} />;
  }
}
