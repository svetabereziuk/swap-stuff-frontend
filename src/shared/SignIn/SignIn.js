import React from 'react';
import { Formik } from "formik";
import { Button, Input } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { gql } from "apollo-boost";
import { useMutation } from "@apollo/react-hooks";

const useStyles = makeStyles(theme => ({
  input: {
    marginRight: "10px"
  }
}));

const LOGIN = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`;

export default () => {
  let classes = useStyles();
  const [login, { error: loginError }] = useMutation(LOGIN);

  return (
    <Formik
      initialValues={{ email: "", password: "" }}
      validate={values => {
        let errors = {};
        if (!values.email) {
          errors.email = "Required";
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = "Invalid email address";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting, setErrors, setFieldError }) => {
        login({ variables: { email: values.email, password: values.password } })
          .then(({ data }) => localStorage.setItem("token", data.login.token))
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
        /* and other goodies */
      }) => (
          <form onSubmit={handleSubmit}>
            <Input
              type="email"
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              className={classes.input}
              placeholder="email"
            />
            <Input
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              className={classes.input}
              placeholder="password"
            />
            {errors.password && touched.password && errors.password}
            <Button type="submit" disabled={isSubmitting}>
              Submit
            </Button>
            <Button onClick={localStorage.removeItem('token')}>
              Sign out
            </Button>

            <div>
              {loginError && loginError.message}
            </div>
            <div>token - {localStorage.getItem('token')}</div>
          </form>
        )}
    </Formik >
  );
}