import React from 'react';
import Card from "../Card/Card";

import { gql } from "apollo-boost";
import { useQuery } from "@apollo/react-hooks";

export default () => {
  const PRODUCTS_QUERY = gql`
    {
      products {
        id
        title
        description
      }
    }
  `;

  const { loading, error, data } = useQuery(PRODUCTS_QUERY);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return (
    data.products.map(product => (
      <Card key={product.id} product={product} />
    ))
  )
}