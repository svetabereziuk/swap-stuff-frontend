import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import {
  Link
} from "react-router-dom";

const useStyles = makeStyles(theme => ({
  link: {
    color: "#fff",
    textDecoration: "none",
    marginLeft: "10px"
  }
}));

export default ({ menu }) => {
  const classes = useStyles();
  return (
    <AppBar position="relative">
      <Toolbar>
        <Typography variant="h6" color="inherit" noWrap>
          {menu.map(menuItem => (
            <Link
              key={menuItem.link}
              className={classes.link}
              to={menuItem.link}
            >
              {menuItem.title}
            </Link>
          ))}
        </Typography>
      </Toolbar>
    </AppBar>
  );
};
