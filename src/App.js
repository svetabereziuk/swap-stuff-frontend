import React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import HeaderMenu from "./shared/HeaderMenu";
import { ApolloClient, HttpLink, InMemoryCache } from "apollo-boost";
import { ApolloProvider } from '@apollo/react-hooks';

import Products from "./shared/Products/Products";
import SignIn from "./shared/SignIn/SignIn";
import Home from "./shared/Home/Home";
import Footer from "./shared/Footer/Footer";
import Promo from "./shared/Promo/Promo";
import { isPunctuatorToken } from "graphql/language/lexer";

const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  }
}));

const token = localStorage.getItem('token');
const client = new ApolloClient({
  link: new HttpLink({
    uri: 'http://localhost:3001/graphql',
    headers: {
      authorization: token
    }
  }),
  cache: new InMemoryCache()
});

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        token ? (
          children
        ) : (
            <Redirect
              to={{
                pathname: "/signin",
                state: { from: location }
              }}
            />
          )
      }
    />
  );
}

export default function App() {
  const classes = useStyles();

  return (
    <Router>
      <ApolloProvider client={client}>
        <CssBaseline />
        <HeaderMenu />
        <main>
          <Promo />
          <Container className={classes.cardGrid} maxWidth="md">
            <Grid container spacing={4}>
              <Switch>
                <Route path="/signin">
                  <SignIn />
                </Route>
                <PrivateRoute path="/products">
                  <Products />
                </PrivateRoute>
                <Route path="/">
                  <Home />
                </Route>
              </Switch>
            </Grid>
          </Container>
        </main>
        <Footer />
      </ApolloProvider>
    </Router>
  );
}
